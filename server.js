var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);

/**** SQL ****/
var sql = require('./sql')('./Modules/mysql');
var pack = require('./Modules/package');

server.on("error", function(e) {
    if (e.code == 'EADDRINUSE') {
        console.log('Unuse :(');
        process.exit();
    }
});
server.listen(3000);

var Mode = require('./Mode/mode');
var Offer = require('./Mode/offer')();

/****
 * USER = { name : "" }
****/
var users = {};

/****
 * GAME = Object
****/
var games = {};
io.on("connection" , function(socket) {
    var me;
    socket.on("INIT" , function(data) {
        id = socket.id;
        sql.module(data.name , function(error, result, fields){
            console.log(result);
            me = { name : name , socketID : id };
            users[name] = { socketID : id };
            pack.answer(socket , "OK");
        });
    });
    
    socket.on("findGame" , function(data) {
       var sheet = Offer(me , data , users);
       if(!sheet) { pack.answer(socket , "ERROR"); return; }
       games[sheet.creator.socketID] = Mode(sheet);
    });
    
    socket.on("connectGame" , function(data) {
        if(!(data.to in users)) { pack.answer(socket , "ERROR"); return; }
        var toID = users[data.to];
        var to = { name : data.to , socketID : toID } , 
            currentParty = games[data];
        if(currentParty.addPlayer(me)) {
            pack.answer(socket , "START");
            currentParty.setStart();
        }else{
            pack.answer(socket , "OK");
        }
    });
    
    socket.on('disconnect', function() {
        users.splice(users.indexOf(3), 1);
    });
});

/*

var host;
var port;

var server = app.listen(3000, function () {
  host = server.address().address
  port = server.address().port
  console.log('Listening at http://%s:%s', host, port)
});

app.get('/' , function(req , res) {
    res.send("This is SweChess!");
});

app.get('/info' , function(req , res) {
    res.send(host + ":" + port);    
});
*/