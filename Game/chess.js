var Bishop = require('../Figures/bishop') , 
    King = require('../Figures/king') , 
    Knight = require('../Figures/knight') , 
    Pawn = require('../Figures/pawn') , 
    Queen = require('../Figures/queen') , 
    Rook = require('../Figures/rook');
    
var Game = require("./game");
require("./figure");

var a = new Game();
a.change();

global.chess = { 
    maxFigures : 6 ,
    w : 8 , 
    h : 8 ,
    white : "WHITE" , black : "BLACK" ,
    avaible : "AVAIBLE" , break_ : "BREAK" , none : "NONE"
};

var Pack = {
    ok : function(pos) {
        if((pos.x > 8)||(pos.x < 1)||(pos.y < 1)||(pos.y > 8))
            return false;
        else return true;
    } , 
    toPos : function(x_ , y_) {
        return ({ x : x_ , y : y_ }); 
    }
}

var def_pos = [
    [null , null , null , null , null , null , null , null , null] ,
    [null ,  { color : "WHITE" , index : 4 }  ,  { color : "WHITE" , index : 3 }  ,  { color : "WHITE" , index : 2 }  ,  { color : "WHITE" , index : 5 }  ,  { color : "WHITE" , index : 6 }  ,  { color : "WHITE" , index : 2 }  ,  { color : "WHITE" , index : 3 }  ,  { color : "WHITE" , index : 4 } ] ,
    [null , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 }  , { color : "WHITE" , index : 1 } ] ,
    [null , null , null , null , null , null , null , null , null] ,
    [null , null , null , null , null , null , null , null , null] ,
    [null , null , null , null , null , null , null , null , null] ,
    [null , null , null , null , null , null , null , null , null] ,
    [null , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 }  , { color : "BLACK" , index : 1 } ] ,
    [null ,  { color : "BLACK" , index : 4 }  ,  { color : "BLACK" , index : 3 }  ,  { color : "BLACK" , index : 2 }  ,  { color : "BLACK" , index : 5 }  ,  { color : "BLACK" , index : 6 }  ,  { color : "BLACK" , index : 2 }  ,  { color : "BLACK" , index : 3 }  ,  { color : "BLACK" , index : 4 } ]
];

var def_color = { white : "WHITE" , black : "BLACK" }; // WHITE and BLACK

/****
0 - EMPTY
1 - PAWN
2 - BISHOP
3 - KNIGHT
4 - ROOK
5 - QUEEN
6 - KING
****/

/* move is { fig , color , from , to } */
/*    from - {x , y}; to is [x , y]    */

function Move(Game , party) {
    return (Game.can(party));
}

function newGame() {
    return (new Game(
        toMap(def_pos)    
    ));
}

function newPosition(MyMap) {
    return (new Game(
        toMap(MyMap)
    ));
}

exports.Move = Move;
exports.newGame = newGame;
exports.newPosition = newPosition;