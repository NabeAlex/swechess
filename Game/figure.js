/****
0 - EMPTY
1 - PAWN
2 - BISHOP
3 - KNIGHT
4 - ROOK
5 - QUEEN
6 - KING
****/

exports.toFigure = function toFigure(index) {
    switch (move.fig) {
            case 1:
                return (new Pawn());
            case 2:
                return (new Bishop());
            case 3:
                return (new Knight());
            case 4:
                return (new Rook());
            case 5:
                return (new Queen());
            case 6:
                return (new King());
            case null:
                return (null);
            default:
                return { status : false , msg : "Wrong figure!" };
    }
};

exports.toMap = function toMap(map) {
    var res = [];
    for(var i = 0; i <= 9; i++) {
        res[i] = [];
        for(var j = 0; j <= 9; j++) {
            res[i][j] = toFigure(map[i][j].index);
            if(res[i][j] !== null) {
                res[i][j].setINIT(Pack.toPos(i , j) , map[i][j].color);
            }
        }
    }
    return (res);
};