var STYLE = { standart : "STANDART" , swedish : "SWEDISH" , coach : "COACH" };

var Offer = function(set) {
    
    /* NOW TYPE */
    var newForm = function() {
        return  ({
            users : [] , 
            mode : "" ,
            creator : "" ,
            friends : [] , /* IN A TEAM (1). NOT (2 , 3) */
            date : null , 
            lifeTime : 60 /* TEST */ ,
            filter : {} /* WHITE OR NOT WHITE */
        });
    }
    return (function(me , info , users) {
        var result = Up();
        var val;
        result.users.push(me);
        result.creator = me;
        for(var human in info.chance) {
            val = info.chance[human];
            if(val in users) {
                result.friends.push({ name : val , socketID : users[val] });
            }
        }
        result.date = Date.now();
        switch (info.style.toUpperCase()) {
            case STYLE.standart:
                result.mode = STYLE.standart;
            case STYLE.swedish :
                result.mode = STYLE.swedish;
            default:
                return false;
        }
        return result;
    });
}

module.exports = Offer;