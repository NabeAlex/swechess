var STYLE = { standart : "STANDART" , swedish : "SWEDISH" , coach : "COACH" };

var Chess = require('../Game/chess')

var Coach = require('./Modes/coach') , 
    Standart = require('./Modes/standart') ,
    Swedish = require('./Modes/swedish');

module.exports = function(info) {
    switch (info.mode) {
        case STYLE.coach:
            return (new Coach(Chess));
        case STYLE.standart:
            return (new Standart(Chess));
        case STYLE.swedish:
            return (new Swedish(Chess));
        default :
            return false;
    }
}