function Knight() {
    var pos;
    var color;
    
    var wasINIT = false;
    
    // UNIQ
    this.Move = function(map) {
        var i , j , tmp , NEW = [];
        for(i = 1; i <= 8; i++) {
            NEW[i] = [];    
            for(j = 1; j <= 8; j++) {
                NEW[i][j] = "NONE";
            }
        }
        for(i = -2; i <= 2; i + 4) {
            for(j = -1; i <= 1; i + 2) {
                tmp = { x : from.x + j , y : from.y + i};
                if(global.chess.ok(tmp)) {
                    if(map[tmp.x][tmp.y] == null)
                        NEW[tmp.x][tmp.y] = "AVAIBLE"
                    else if(map[tmp.x][tmp.y].getColor() != color)
                        NEW[tmp.x][tmp.y] = "BREAK"
                }
                tmp = { x : from.x + i , y : from.y + j};
                if(global.chess.ok(tmp)) {
                    if(map[tmp.x][tmp.y] == null)
                        NEW[tmp.x][tmp.y] = "AVAIBLE"
                    else if(map[tmp.x][tmp.y].getColor() != color)
                        NEW[tmp.x][tmp.y] = "BREAK"
                }
            }
        }
        return NEW;
    }
    
    // CONST
    this.index = 6;
    
    this.setINIT = function(pos_ , color_) {
        if(wasINIT == true) return;
        pos = pos_;
        color = color_;
        wasINIT = true;
    }
    
    this.changePosition = function(pos_) {
        pos = pos_;
        
    }
}

module.exports = Knight;