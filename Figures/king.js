function King(color_ , pos_) {
    var from = pos_;
    var color = color_;
    this.index = 6;
    
    this.getColor = function() { return color; }
    this.getPos = function() { return pos_; }
    
    // UNIQ
    this.WasMove = false;
    
    this.Move = function(map) {
        var NEW = [] , i , j;
        for(i = 1; i <= 8; i++) {
            NEW[i] = [];    
            for(j = 1; j <= 8; j++) {
                NEW[i][j] = "NONE";
            }
        }
        
        for(i = -1; i <= 1; i++) {
            for(j = -1; j <= 1; j++) {
                if((i !== 0)&&(j !== 0)) {
                    if(map[form.x + i][from.y + j] == null) 
                        NEW[form.x + i][from.y + j] = "AVAIBLE";
                    else(map[form.x + i][from.y + j].getColor() != color)
                        NEW[form.x + i][from.y + j] = "BREAK";
                }
            }
        }
        return NEW;
    }
    
    this.Update = function() { }
    this.Moved = function(pos) { this.WasMove = true; }
    this.Delete = function() { }
}

module.exports = King;  