module.exports = function(module_) {
    var mysql = require('mysql');

    var client = mysql.createClient();
    client.host='127.0.0.1';
    client.port= '3306';
    client.user='someuser';
    client.password='userpass';
    client.database='node';
    
    this.query = function(q , callback) {
        client.query(q , callback);
    }
    
    this.endClient = function() {
        client.end();
    }
    
    this.module = require(module_)(client);
}

